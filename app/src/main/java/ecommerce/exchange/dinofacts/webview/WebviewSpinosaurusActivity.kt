package ecommerce.exchange.dinofacts.webview

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.webkit.WebView
import android.webkit.WebViewClient
import ecommerce.exchange.dinofacts.R

private lateinit var webView: WebView

class WebviewSpinosaurusActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.spinosaurus_webview)
        if (supportActionBar != null)
            supportActionBar?.hide()

        webView = findViewById(R.id.webview)
        webView.settings.setJavaScriptEnabled(true)

        webView.webViewClient = object : WebViewClient() {
            override fun shouldOverrideUrlLoading(view: WebView?, url: String?): Boolean {
                if (url != null) {
                    view?.loadUrl(url)
                }
                return true
            }
        }
        webView.loadUrl("https://en.wikipedia.org/wiki/Spinosaurus")
    }
}
package ecommerce.exchange.dinofacts

import android.content.Intent
import android.os.Bundle
import android.util.DisplayMetrics
import android.widget.Button
import android.widget.ImageButton
import android.widget.TextView
import androidx.appcompat.app.AppCompatActivity
import ecommerce.exchange.dinofacts.webscrape.*
import ecommerce.exchange.dinofacts.webview.*


open class MainActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        if (supportActionBar != null)
            supportActionBar?.hide()

        /*
        val dinogame = findViewById<Button>(R.id.dinogame)
        dinogame.setOnClickListener{
            val intent = Intent(this, DinoGame::class.java)
            startActivity(intent)
        } */

        val pterodactyl = findViewById<ImageButton>(R.id.pterodactyl)
        pterodactyl.setOnClickListener{
            val intent = Intent(this, WebscrapePterodactylActivity::class.java)
            startActivity(intent)
        }

        val silvisaurus = findViewById<ImageButton>(R.id.silvisaurus)
        silvisaurus.setOnClickListener{
            val intent = Intent(this, WebscrapeSilvisaurusActivity::class.java)
            startActivity(intent)
        }

        val lirainosaurus = findViewById<ImageButton>(R.id.lirainosaurus)
        lirainosaurus.setOnClickListener{
            val intent = Intent(this, WebscrapeLirainosaurusActivity::class.java)
            startActivity(intent)
        }

        val iguanodone = findViewById<ImageButton>(R.id.iguanodone)
        iguanodone.setOnClickListener{
            val intent = Intent(this, WebscrapeIguanodoneActivity::class.java)
            startActivity(intent)
        }

        val brontosaurus = findViewById<ImageButton>(R.id.brontosaurus)
        brontosaurus.setOnClickListener{
            val intent = Intent(this, WebscrapeBrontosaurusActivity::class.java)
            startActivity(intent)
        }

        val gallimimus = findViewById<ImageButton>(R.id.gallimimus)
        gallimimus.setOnClickListener{
            val intent = Intent(this, WebscrapeGallimimusActivity::class.java)
            startActivity(intent)
        }

        val Isanosaurus = findViewById<ImageButton>(R.id.isanosaurus)
        Isanosaurus.setOnClickListener{
            val intent = Intent(this, WebscrapeIsanosaurusActivity::class.java)
            startActivity(intent)
        }

        val ichthyosaurus = findViewById<ImageButton>(R.id.ichthyosaurus)
        ichthyosaurus.setOnClickListener{
            val intent = Intent(this, WebscrapeIchthyosaurusActivity::class.java)
            startActivity(intent)
        }

        val mosasaurus = findViewById<ImageButton>(R.id.mosasaurus)
        mosasaurus.setOnClickListener{
            val intent = Intent(this, WebscrapeMosasaurusActivity::class.java)
            startActivity(intent)
        }

        val diplodocus = findViewById<ImageButton>(R.id.diplodocus)
        diplodocus.setOnClickListener{
            val intent = Intent(this, WebscrapeDiplodocusActivity::class.java)
            startActivity(intent)
        }

        val tyrannosaurus = findViewById<ImageButton>(R.id.tyrannosaurus)
        tyrannosaurus.setOnClickListener{
            val intent = Intent(this, WebscrapeTyrannosaurusActivity::class.java)
            startActivity(intent)
        }

        val styracosaurus = findViewById<ImageButton>(R.id.styracosaurus)
        styracosaurus.setOnClickListener{
            val intent = Intent(this, WebscrapeStyracosaurusActivity::class.java)
            startActivity(intent)
        }

        val baryonyx = findViewById<ImageButton>(R.id.baryonyx)
        baryonyx.setOnClickListener{
            val intent = Intent(this, WebscrapeBaryonyxActivity::class.java)
            startActivity(intent)
        }

        val raptor = findViewById<ImageButton>(R.id.raptor)
        raptor.setOnClickListener{
            val intent = Intent(this, WebscrapeRaptorActivity::class.java)
            startActivity(intent)
        }

        val stegosaurus = findViewById<ImageButton>(R.id.stegosaurus)
        stegosaurus.setOnClickListener{
            val intent = Intent(this, WebscrapeStegosaurusActivity::class.java)
            startActivity(intent)
        }

        val carnotaurus = findViewById<ImageButton>(R.id.carnotaurus)
        carnotaurus.setOnClickListener{
            val intent = Intent(this, WebscrapeCarnotaurusActivity::class.java)
            startActivity(intent)
        }

        val tsintaosaurus = findViewById<ImageButton>(R.id.tsintaosaurus)
        tsintaosaurus.setOnClickListener{
            val intent = Intent(this, WebscrapeTsintaosaurusActivity::class.java)
            startActivity(intent)
        }

        val ankilosaurus = findViewById<ImageButton>(R.id.ankilosaurus)
        ankilosaurus.setOnClickListener{
            val intent = Intent(this, WebscrapeAnkilosaurusActivity::class.java)
            startActivity(intent)
        }

        val parasaurolophus = findViewById<ImageButton>(R.id.parasaurolophus)
        parasaurolophus.setOnClickListener{
            val intent = Intent(this, WebscrapeParasaurolophusActivity::class.java)
            startActivity(intent)
        }

        val europasaurus = findViewById<ImageButton>(R.id.europasaurus)
        europasaurus.setOnClickListener{
            val intent = Intent(this, WebscrapeEuropasaurusActivity::class.java)
            startActivity(intent)
        }

        val andesaurus = findViewById<ImageButton>(R.id.andesaurus)
        andesaurus.setOnClickListener{
            val intent = Intent(this, WebscrapeAndesaurusActivity::class.java)
            startActivity(intent)
        }

        val cuelophysis = findViewById<ImageButton>(R.id.cuelophysis)
        cuelophysis.setOnClickListener{
            val intent = Intent(this, WebscrapeCuelophysisActivity::class.java)
            startActivity(intent)
        }

        val allosaurus = findViewById<ImageButton>(R.id.allosaurus)
        allosaurus.setOnClickListener{
            val intent = Intent(this, WebscrapeAllosaurusActivity::class.java)
            startActivity(intent)
        }

        val spinosaurus = findViewById<ImageButton>(R.id.spinosaurus)
        spinosaurus.setOnClickListener{
            val intent = Intent(this, WebscrapeSpinosaurusActivity::class.java)
            startActivity(intent)
        }

        val brachinosaurus = findViewById<ImageButton>(R.id.brachinosaurus)
        brachinosaurus.setOnClickListener{
            val intent = Intent(this, WebscrapeBrachinosaurusActivity::class.java)
            startActivity(intent)
        }
    }
}
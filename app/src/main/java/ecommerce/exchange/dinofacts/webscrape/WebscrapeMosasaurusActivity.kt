package ecommerce.exchange.dinofacts.webscrape

import android.os.Bundle
import android.view.View
import android.widget.Button
import android.widget.ImageView
import android.widget.TextView
import androidx.appcompat.app.AppCompatActivity
import com.squareup.picasso.Picasso
import ecommerce.exchange.dinofacts.R
import org.jetbrains.anko.doAsync
import org.jetbrains.anko.uiThread
import org.jsoup.Jsoup
import org.jsoup.nodes.Document
import org.jsoup.select.Elements
import java.io.IOException
import java.util.*
import kotlin.collections.ArrayList

class WebscrapeMosasaurusActivity : AppCompatActivity() {

    private lateinit var textViewTitle: TextView
    private lateinit var textView: TextView
    private lateinit var altText: TextView
    private lateinit var imageView: ImageView
    private lateinit var lowerDivider: View
    private lateinit var nextButton: Button
    lateinit var words: String

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.mosasaurus_webscrape)
        if (supportActionBar != null)
            supportActionBar?.hide()

        textViewTitle = findViewById(R.id.textViewTitle)
        textView = findViewById(R.id.textView)
        imageView = findViewById(R.id.imageView)
        lowerDivider = findViewById(R.id.lowerDivider)
        nextButton = findViewById(R.id.nextButton)
        altText = findViewById(R.id.altText)

        var loaded = true

        var urlsList = ArrayList<String>()
        var altList = ArrayList<String>()
        var index = 0

        doAsync {
            val result = Jsoup.connect("https://en.wikipedia.org/wiki/Mosasaurus").get()
            words = result.select("div#mw-content-text p").text().toString()

            val regexPattern = "\\[\\d+\\]"
            val regex = regexPattern.toRegex()
            words = words.replace(regex, "")

            val document: Document = Jsoup.parse(result.html())
            val images: Elements = document.select("img")

            uiThread {

                for (image in images) {
                    val imageUrl: String = "https:" + image.attr("src")
                    val imageAlt: String = image.attr("alt")
                    if (!imageUrl.contains("Mosasaurus") || imageUrl.contains("70px")) {
                        continue
                    }
                    urlsList.add(imageUrl)
                    altList.add(imageAlt)
                }

                fun populateImage() {
                    loaded = false
                    if (index >= urlsList.size) {
                        index = 0
                    }

                    Picasso
                            .with(this@WebscrapeMosasaurusActivity)
                            .load(urlsList.get(index))
                            .resize(600, 400)
                            .into(imageView)

                    nextButton.visibility = View.VISIBLE
                    lowerDivider.visibility = View.VISIBLE

                    if (altList.get(index).isEmpty()) {
                        altText.visibility = View.INVISIBLE
                    } else {
                        altText.setText(altList.get(index))
                        altText.visibility = View.VISIBLE
                    }

                    index++
                    loaded = true
                }

                populateImage() //call this so image is populated when page loads

                nextButton.setOnClickListener {
                    if (loaded) {
                        populateImage()
                    } else {
                        while (!loaded) {
                            Thread.sleep(100)
                        }
                    }
                }
                textView.text = words
            }
        }
    }
}